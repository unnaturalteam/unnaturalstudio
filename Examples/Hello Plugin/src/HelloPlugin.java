import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.JBPopupFactory;

/**
 * Created by Lyle on 9/19/2015.
 */
public class HelloPlugin extends AnAction {
    public void actionPerformed(AnActionEvent e) {
        final Project project = e.getProject();
        if (project == null) {
            return;
        }
        Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
        if (editor == null) {
            return;
        }
        final Document document = editor.getDocument();
        if (document == null) {
            return;
        }
        String doc = document.getText();
        JBPopupFactory popup = JBPopupFactory.getInstance();
        if (doc.contains("hello plugin")) {
            popup.createMessage("Hello World!").showCenteredInCurrentWindow(project);
        } else {
            popup.createMessage("No one said hello to me...").showCenteredInCurrentWindow(project);
        }
    }
}
