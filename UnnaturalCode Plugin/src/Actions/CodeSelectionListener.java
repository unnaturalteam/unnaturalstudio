package Actions;

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.event.SelectionEvent;
import com.intellij.openapi.editor.event.SelectionListener;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.wm.StatusBar;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.ui.awt.RelativePoint;
import util.Domain.Token;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;

import java.util.ArrayList;

/**
 * Created by timotei on 2015-12-03.
 * This class calculates the entropy of a selected block of text
 */
public class CodeSelectionListener implements SelectionListener {

    private final Project project;
    private final Editor selectedTextEditor;

    /**
     * Constructor that creates the selection listener
     * @param selectedTextEditor current selected text editor
     */
    public CodeSelectionListener(Editor selectedTextEditor) {
        this.project = UnnaturalProgram.getProject();
        this.selectedTextEditor = selectedTextEditor;
    }
    /**
     * Called when the selected area in an editor is changed.
     *
     * @param e the event containing information about the change.
     */
    @Override
    public void selectionChanged(SelectionEvent e) {
        try {
            Integer selectionStart = this.selectedTextEditor.getSelectionModel().getSelectionStart();
            Integer selectionEnd = this.selectedTextEditor.getSelectionModel().getSelectionEnd();
            TokenizedCode code = UnnaturalProgram.getTokenizedCode(this.selectedTextEditor);
            ArrayList<Token> allTokens = code.getTokenList();

            if (selectionEnd == -1) {
                selectionEnd = allTokens.size() - 1;
            }
            if (selectionStart == -1) {
                selectionStart = allTokens.size() - 1;
            }

            Integer startOffset;
            Integer endOffset;

            try {
                startOffset = code.getTokenIndexFromOffset(selectionStart);
                endOffset = code.getTokenIndexFromOffset(selectionEnd);
            } catch (Exception e1) {
                return;
            }

            Double totalEntropy = 0.0;
            for (int i = startOffset; i <= endOffset; i++) {
                if (allTokens.get(i).getEntropy() == -1) continue;
                if (allTokens.size() < i) continue;
                totalEntropy += allTokens.get(i).getEntropy();
            }

            // In case only one token was selected, then we don't average antyhing, as the result of total/(start-end)
            // is total/0 which  gives us 'infinity'
            if (endOffset != startOffset) {
                totalEntropy = totalEntropy / (endOffset - startOffset);
            }

            if (totalEntropy > 0) {
                StatusBar statusBar = WindowManager.getInstance()
                        .getStatusBar(project);

                JBPopupFactory.getInstance()
                        .createHtmlTextBalloonBuilder(String.format("Selected text entropy: %.2f", totalEntropy), MessageType.INFO, null)
                        .setFadeoutTime(7500)
                        .createBalloon()
                        .show(RelativePoint.getCenterOf(statusBar.getComponent()),
                                Balloon.Position.atRight);
            }
        }
        catch (Exception e1)
        {
            System.err.println(e1);
        }
    }
}
