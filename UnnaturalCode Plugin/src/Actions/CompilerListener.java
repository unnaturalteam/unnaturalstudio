package Actions;

import UI.CompilationErrorPopup;
import com.intellij.openapi.compiler.CompilationStatusListener;
import com.intellij.openapi.compiler.*;
import com.intellij.openapi.project.*;
import conf.Config;
import util.Access.RESTServer;
import util.Access.UnnaturalDB;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;
import util.ProjectFileManager;
import util.Research.ResearchObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Listens to IntelliJ for when it had a successful compile so it can create
 * a popup for the user to interpret.
 * Created by Lyle on 10/11/2015.
 */
public class CompilerListener implements CompilationStatusListener{

    private final Project project;

    public CompilerListener() {
        this.project = UnnaturalProgram.getProject();
    }

    /**
     * put the following into the main program
     * CompilerListener compilationListener = new CompilerListener();
     * CompilerManager.getInstance(project).addCompilationStatusListener(compilationListener);
     */

    @Override
    public void compilationFinished(boolean aborted, int errors, int warnings, CompileContext compileContext) {
        CompileScope cs = compileContext.getCompileScope();
        if (errors > 0) {
            //System.out.print(cs.toString());
            CompilationErrorPopup.main(project);
        }

        ResearchObject.getInstance().buildObject(compileContext, project);

        if (!aborted && errors == 0) {
            try {
                String projectpath = ProjectFileManager.getFilePath(project.getProjectFilePath(), 2);
                ArrayList<String> files = ProjectFileManager.getJavaFileContents(projectpath);
                for (String file : files) {
                    UnnaturalDB.storeFile(file);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            TokenizedCode code = UnnaturalProgram.getTokenizedCode(UnnaturalProgram.getCurrentEditor());
            if (code.isHighlighted()) {
                code.resetEntropies();
                code.refreshEntropy();
            }
        }

    }

    @Override
    public void fileGenerated(String outputRoot, String relativePath) {

    }
}

