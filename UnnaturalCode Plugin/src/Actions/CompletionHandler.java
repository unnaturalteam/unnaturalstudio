package Actions;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.patterns.PsiElementPattern;
import com.intellij.util.ProcessingContext;
import conf.Config;
import org.jetbrains.annotations.NotNull;
import util.Access.RESTServer;
import util.Domain.Suggestion;
import util.Domain.Token;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;
import util.Research.ResearchObject;
import util.Research.SuggestionsPackage;

import java.util.ArrayList;
import java.util.Objects;


/**
 * Created by noah on 2015-11-26.
 */
// http://www.jetbrains.org/intellij/sdk/docs/tutorials/custom_language_support/completion_contributor.html
// https://www.youtube.com/watch?v=UBxuj2ToizY
// https://devnet.jetbrains.com/message/5520002#5520002
public class CompletionHandler extends CompletionContributor {

    public CompletionHandler() {
        super();
        PsiElementPattern pattern = PlatformPatterns.psiElement();
        CompletionProvider<CompletionParameters> cp = getCompletionProvider();

        extend(CompletionType.BASIC, pattern, cp);
    }



    public CompletionProvider<CompletionParameters> getCompletionProvider() {
        final Editor editor = FileEditorManager.getInstance(UnnaturalProgram.getProject()).getSelectedTextEditor();
        return new CompletionProvider<CompletionParameters>() {
            public void addCompletions(@NotNull CompletionParameters parameters,
                                       ProcessingContext context,
                                       @NotNull CompletionResultSet resultSet) {
                HandleSuggestions(resultSet, editor);
            }

        };
    }

    private void HandleSuggestions(@NotNull CompletionResultSet resultSet, Editor editor) {
        CaretModel caretModel = editor.getCaretModel();
        TokenizedCode tc = UnnaturalProgram.getTokenizedCode(editor);
        ArrayList<Token> tokenList = tc.getUpdatedTokenList();
        if (tokenList.size() == 0) {
            resultSet.addElement(LookupElementBuilder.create("").withTypeText("UnnaturalCode").withTailText("No Suggestions Found"));
        }
        else {
            int offset = caretModel.getOffset();
            int endToken = tc.getTokenIndexFromOffset(tokenList,offset);
            int startToken = endToken - 20;
            if (startToken < 0) {
                startToken = 0;
            }
            if (endToken != 0) {

                if (Objects.equals(tokenList.get(endToken).getTokenType(), "WHITE_SPACE")) {
                    endToken--;
                }
            }

            RESTServer rs = new RESTServer(Config.getUrl());
            String jsonTokens = tc.getJSONArrayString(tokenList,startToken,endToken);
            try {
                ArrayList<Suggestion> suggestions = rs.getSuggestions(jsonTokens);
                CreateSuggestionsPackage(tc, tokenList, offset, suggestions);
                for (Suggestion s : suggestions) {
                    String prediction = "";
                    for (String p: s.getSuggestions()) {
                        prediction += p + " ";
                    }
                    resultSet.addElement(LookupElementBuilder.create(prediction).withTypeText("UnnaturalCode"));
                }
            }
            catch (Exception ignored) {}
            rs.close();
        }
    }

    private void CreateSuggestionsPackage(TokenizedCode tc, ArrayList<Token> tokenList, int offset, ArrayList<Suggestion> suggestions) {
        ArrayList<SuggestionsPackage> roSuggestions= ResearchObject.getInstance().getAllSuggestions();
        SuggestionsPackage suggestionsPackage = new SuggestionsPackage();
        suggestionsPackage.setSuggestions(suggestions);
        suggestionsPackage.setLocation(offset);
        suggestionsPackage.setTokens_before(tc.getTokensBefore(tokenList,offset));
        suggestionsPackage.setTokens_after(tc.getTokensAfter(tokenList,offset));
        roSuggestions.add(suggestionsPackage);
        ResearchObject.getInstance().setAllSuggestions(roSuggestions);
    }
}


