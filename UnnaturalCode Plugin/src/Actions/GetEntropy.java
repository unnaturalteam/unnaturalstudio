package Actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import util.Access.ResearchServer;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;

import java.awt.event.KeyListener;

/**
 * This class gets the text editor and passes it to the
 * TokenizedCode class and calls TokenizedCode to get the code
 * Entropy highlighted in the IDE.
 * Created by Lyle on 9/23/2015.
 */
public class GetEntropy extends AnAction {
    public void actionPerformed(AnActionEvent e) {
        final Project project = e.getProject();
        if (project == null) {
            return;
        }
        final Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
        if (editor == null) {
            return;
        }

        TokenizedCode code = UnnaturalProgram.getTokenizedCode(editor);
        code.refreshEntropy();
    }
}
