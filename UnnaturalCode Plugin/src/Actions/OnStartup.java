package Actions;


import com.intellij.codeInsight.daemon.DaemonCodeAnalyzer;
import com.intellij.codeInsight.hint.EditorHintListener;
import com.intellij.codeInsight.hint.HintManagerImpl;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diff.impl.incrementalMerge.ui.EditorPlace;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.event.SelectionEvent;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerAdapter;
import com.intellij.openapi.fileEditor.FileEditorManagerEvent;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.startup.StartupActivity;
import com.intellij.openapi.project.Project;

import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.LightweightHint;
import org.jetbrains.annotations.NotNull;
import util.Access.ResearchServer;
import util.Access.UnnaturalDB;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;
import util.Threads.UpdateThread;


import java.util.ArrayList;


/**
 * Created by noah on 2015-11-15.
 */
public class OnStartup implements StartupActivity {

    @Override
    public void runActivity(@NotNull Project project) {
        ResearchServer.establishConnection();
        UnnaturalProgram.setProject(project);
        UnnaturalProgram.initCompilerListener();
        //UnnaturalProgram.initSelectionListener();
        UnnaturalDB.initDB();
        UnnaturalProgram.initTokenListener();
        TokenizedCode code = UnnaturalProgram.getTokenizedCode(UnnaturalProgram.getCurrentEditor());
        UnnaturalProgram.invokeUpdate(new UpdateThread(code, false));

        Editor editor =  FileEditorManager.getInstance(project).getSelectedTextEditor();
        if (editor != null)
        {
            CodeSelectionListener csl = new CodeSelectionListener(editor);
            editor.getSelectionModel().addSelectionListener(csl);
        }

        project.getMessageBus().connect(project).subscribe(FileEditorManagerListener.FILE_EDITOR_MANAGER,new FileEditorManagerAdapter() {
                    CodeSelectionListener csl;
                    @Override
                    public void fileOpened(@NotNull FileEditorManager source, @NotNull VirtualFile file) {
                        csl = new CodeSelectionListener(source.getSelectedTextEditor());
                        source.getSelectedTextEditor().getSelectionModel().addSelectionListener(csl);
                        UnnaturalProgram.removeTokenListener();
                        UnnaturalProgram.initTokenListener();
                    }

                    @Override
                    public void selectionChanged(@NotNull FileEditorManagerEvent event) {
                        try
                        {
                            csl = new CodeSelectionListener(event.getManager().getSelectedTextEditor());
                            event.getManager().getSelectedTextEditor().getSelectionModel().addSelectionListener(csl);
                            UnnaturalProgram.removeTokenListener();
                            UnnaturalProgram.initTokenListener();
                            TokenizedCode code = UnnaturalProgram.getTokenizedCode(UnnaturalProgram.getCurrentEditor());
                            UnnaturalProgram.invokeUpdate(new UpdateThread(code, code.isHighlighted()));
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }
        );
    //    SuggestionListener sg = new SuggestionListener();
    }
}
