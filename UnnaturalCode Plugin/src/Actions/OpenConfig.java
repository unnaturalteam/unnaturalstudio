package Actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import UI.ConfigView;

/**
 *  Opens UnnaturalStudio's configuration window
 *  Created by Noah on 2015-10-17.
 */
public class OpenConfig  extends AnAction {
    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {
        new ConfigView();
    }
}
