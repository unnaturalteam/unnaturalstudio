package Actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;

/**
 * This class gets the text editor and passes it to the
 * TokenizedCode class and calls TokenizedCode to get the code
 * Entropy highlighting removed in the IDE.
 * Created by Lyle on 10/20/2015.
 */
public class RemoveHighlighting extends AnAction {
    public void actionPerformed(AnActionEvent e) {
        final Project project = e.getProject();
        if (project == null) {
            return;
        }
        final Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
        if (editor == null) {
            return;
        }
        TokenizedCode code = UnnaturalProgram.getTokenizedCode(editor);
        UnnaturalProgram.cancelUpdate();
        code.removeHighlighting();
        code.resetEntropies();
    }
}
