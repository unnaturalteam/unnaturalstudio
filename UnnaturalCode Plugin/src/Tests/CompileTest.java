package Tests;

import conf.Config;
import junit.framework.TestCase;
import util.Access.RESTServer;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;
import util.ProjectFileManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by JasonQi on 2015/12/1.
 */
public class CompileTest extends TestCase {
    private static String dblocation;

    public void testContent() throws IOException {
        String content = "public class Main {\n" +
                "\n" +
                "    public static void main(String[] args) {\n" +
                "        System.out.println(\"Hello World!\");\n" +
                "    }\n" +
                "}\n";
        new File("D:\\test").mkdir();
        File file = new File("D:\\test\\test.java");
        if (!file.exists()) {
            file.createNewFile();
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();
        }

        ArrayList<String> files = ProjectFileManager.getJavaFileContents("D:\\test");
        assertEquals(content, files.get(0));
    }

    public void testData() throws IOException {
        String content = "public class Main {\n" +
                "\n" +
                "    public static void main(String[] args) {\n" +
                "        System.out.println(\"Hello World!\");\n" +
                "    }\n" +
                "}\n";

        String content1 = "public class Main {\n" +
                "\n" +
                "    public static void main(String[] args) {\n" +
                "        System.out.print(\"Hello World!\");\n" +
                "    }\n" +
                "}\n";

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            System.err.println("Could not find sqlite JDBC library");
        }
        try {
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + "D:\\test" + "\\compilation_record.db");
            String query = "CREATE TABLE IF NOT EXISTS FileData(" +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "content TEXT)";
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
            con.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }

        new File("D:\\test").mkdir();
        File file = new File("D:\\test\\test.java");
        File file1 = new File("D:\\test\\test1.java");
        if (!file.exists()) {
            file.createNewFile();
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();
        }

        if (!file1.exists()) {
            file1.createNewFile();
            FileWriter fw = new FileWriter(file1.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content1);
            bw.close();
        }

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            System.err.println("Could not find sqlite JDBC library");
        }
        try {
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + "D:\\test" + "\\compilation_record.db");
            String query = "INSERT INTO FileData(content) VALUES('" + content + "')";
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            query = "INSERT INTO FileData(content) VALUES('" + content1 + "')";
            stmt = con.createStatement();
            stmt.executeUpdate(query);
            query = "SELECT content FROM FileData";
            stmt = con.createStatement();
            ResultSet rs =  stmt.executeQuery(query);
            ArrayList<String> contest= new ArrayList<String>();
            while (rs.next()) {
                contest.add(rs.getString(1));
            }
            TokenizedCode code = UnnaturalProgram.getTokenizedCode(contest.get(contest.size()-2));
            RESTServer rserver = new RESTServer(Config.getUrl());
            System.out.println(rserver.Train(code));
            assertEquals( rserver.Train(code), 202);
            rserver.close();
;           stmt.close();
            con.close();
            //assertEquals(content, contest.get(contest.size()-2));
        } catch (SQLException e) {
            System.err.println("storefile: " + e.getMessage());
        }

    }

}
