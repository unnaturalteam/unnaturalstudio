package Tests;

import com.intellij.openapi.fileEditor.FileEditorManager;
import junit.framework.TestCase;
import util.Domain.Token;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;

import java.util.ArrayList;

/**
 * Created by Thomas Curnow on 20/11/2015.
 */
public class TokenizedCodeTest extends TestCase {

    public void testEntropy() {

        TokenizedCode tc = new TokenizedCode("[document:IDENTIFIER at 23-23(773-781)|5.84948471779799,  :WHITE_SPACE at 23-31(781-782)|5.84948471779799, =:EQ at 23-32(782-783)|5.84948471779799,  :WHITE_SPACE at 23-33(783-784)|5.84948471779799, editor:IDENTIFIER at 23-34(784-790)|5.84948471779799, .:DOT at 23-40(790-791)|5.84948471779799, getDocument:IDENTIFIER at 23-41(791-802)|5.84948471779799, (:LPARENTH at 23-52(802-803)|2.4731418082084127, ):RPARENTH at 23-53(803-804)|2.4731418082084127, ;:SEMICOLON at 23-54(804-805)|2.4731418082084127, \n" +
                "        :WHITE_SPACE at 23-55(805-814)|2.4731418082084127, if:IF_KEYWORD at 24-8(814-816)|2.4731418082084127,  :WHITE_SPACE at 24-10(816-817)|2.4731418082084127, (:LPARENTH at 24-11(817-818)|2.4731418082084127, document:IDENTIFIER at 24-12(818-826)|2.4731418082084127,  :WHITE_SPACE at 24-20(826-827)|2.4731418082084127, ==:EQEQ at 24-21(827-829)|2.4731418082084127,  :WHITE_SPACE at 24-23(829-830)|2.4731418082084127, null:NULL_KEYWORD at 24-24(830-834)|2.4731418082084127, ):RPARENTH at 24-28(834-835)|5.849484717797992,  :WHITE_SPACE at 24-29(835-836)|9.22582762738757, {:LBRACE at 24-30(836-837)|12.602170536977152, \n" +
                "            :WHITE_SPACE at 24-31(837-850)|15.978513446566732, return:RETURN_KEYWORD at 25-12(850-856)|19.35485635615631, ;:SEMICOLON at 25-18(856-857)|22.73119926574589, \n" +
                "        :WHITE_SPACE at 25-19(857-866)|26.10754217533547, }:RBRACE at 26-8(866-867)|29.48388508492505, \n" +
                "        :WHITE_SPACE at 26-9(867-876)|32.86022799451463, fosspayklj:IDENTIFIER at 27-8(876-886)|36.236570904104205, \n" +
                "        :WHITE_SPACE at 27-18(886-895)|36.236570904104205, String:IDENTIFIER at 28-8(895-901)|36.236570904104205,  :WHITE_SPACE at 28-14(901-902)|36.236570904104205, foo:IDENTIFIER at 28-15(902-905)|36.23657090410421,  :WHITE_SPACE at 28-18(905-906)|36.236570904104205, =:EQ at 28-19(906-907)|36.23657090410421,  :WHITE_SPACE at 28-20(907-908)|36.23657090410421, \"num\":STRING_LITERAL at 28-21(908-913)|36.23657090410421, ;:SEMICOLON at 28-26(913-914)|36.23657090410421, \n" +
                "        :WHITE_SPACE at 28-27(914-923)|36.23657090410421]");
        tc.calculateEntropies();
        ArrayList<Token> tokens = tc.getTokenList();
        //System.out.println(tokens.size());
        //System.out.println("test stuff");
        //System.out.println(tokens.get(0).getEntropy());
        assertTrue(tokens.get(0).getEntropy() > 0);
    }
}