package Tests;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.impl.DocumentImpl;
import junit.framework.TestCase;
import util.Domain.Token;
import util.UnnaturalJavaLexer;

import java.util.ArrayList;

/**
 * Test the lexer
 * Created by Lyle on 11/18/2015.
 */
public class lexerTest extends TestCase {

    public void testLexer() {
        String input = "id .,(){}[]+-*/";
        String[] output = {
                "id:IDENTIFIER at 1-0(0-2)|-1.0",
                " :WHITE_SPACE at 1-2(2-3)|-1.0",
                ".:DOT at 1-3(3-4)|-1.0",
                ",:COMMA at 1-4(4-5)|-1.0",
                "(:LPARENTH at 1-5(5-6)|-1.0",
                "):RPARENTH at 1-6(6-7)|-1.0",
                "{:LBRACE at 1-7(7-8)|-1.0",
                "}:RBRACE at 1-8(8-9)|-1.0",
                "[:LBRACKET at 1-9(9-10)|-1.0",
                "]:RBRACKET at 1-10(10-11)|-1.0",
                "+:PLUS at 1-11(11-12)|-1.0",
                "-:MINUS at 1-12(12-13)|-1.0",
                "*:ASTERISK at 1-13(13-14)|-1.0",
                "/:DIV at 1-14(14-15)|-1.0"
        };
        Document doc = new DocumentImpl(input);
        ArrayList<Token> tokens = new UnnaturalJavaLexer(doc).getTokenList();

        assertEquals(output.length, tokens.size());
        for (int i=0; i<tokens.size(); i++) {
            assertEquals(tokens.get(i).toString(), output[i]);
        }
    }
}
