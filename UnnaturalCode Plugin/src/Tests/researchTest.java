package Tests;

import com.google.gson.Gson;
import conf.Config;
import junit.framework.TestCase;
import org.elasticsearch.action.get.GetResponse;
import util.Access.ResearchServer;
import util.Research.ResearchObject;

/**
 * Created by noah on 2015-11-19.
 */
public class researchTest extends TestCase {

    public void testOptIn() {
        Config.setOptIn(Config.OPT_IN);
        ResearchObject.getInstance().setSucceeded(true);
        ResearchServer.setUsername("es_admin");
        ResearchServer.setPassword("password");
        ResearchServer.establishConnection();
        ResearchServer.sendData("test", ResearchObject.getInstance(), "optIn");
        GetResponse data = ResearchServer.getData("test", ResearchObject.getInstance(), "optIn");
        ResearchServer.closeConnection();
        assertTrue(new Gson().toJson(ResearchObject.getInstance()).equals(data.getSourceAsString()));
    }

    public void testOptOut() {
        Config.setOptIn(Config.OPT_OUT);
        ResearchObject.getInstance().setSucceeded(true);
        ResearchServer.setUsername("es_admin");
        ResearchServer.setPassword("password");
        ResearchServer.establishConnection();
        ResearchServer.sendData("test", ResearchObject.getInstance(), "optOut");
        GetResponse data = ResearchServer.getData("test", ResearchObject.getInstance(), "optOut");
        ResearchServer.closeConnection();
        assertFalse(new Gson().toJson(ResearchObject.getInstance()).equals(data.getSourceAsString()));
    }
}
