package Tests;

import com.intellij.openapi.editor.impl.DocumentImpl;
import junit.framework.TestCase;
import util.Difference;
import util.Domain.Token;
import util.Domain.UnnaturalProgram;
import util.UnnaturalJavaLexer;

import java.util.ArrayList;

/**
 * Test for Difference and Token methods
 * Created by Lyle on 11/18/2015.
 */
public class updateTest extends TestCase {

    public void testUpdate() {
        String base = "System.out.println();";
        String insert = "System.out.println(\"test\");";
        String replace = "System.out.println(\"test2\");";
        String delete = "System.out.println();";

        ArrayList<Token> tokens = new ArrayList<Token>();
        UnnaturalJavaLexer lexer = new UnnaturalJavaLexer(new DocumentImpl(base));
        ArrayList<Token> newtokens = lexer.getTokenList();
        new Difference(tokens, newtokens).syncDifference();
        validate(tokens, newtokens);

        lexer.setDocument(new DocumentImpl(insert));
        newtokens = lexer.getTokenList();
        new Difference(tokens, newtokens).syncDifference();
        validate(tokens, newtokens);

        lexer.setDocument(new DocumentImpl(replace));
        newtokens = lexer.getTokenList();
        new Difference(tokens, newtokens).syncDifference();
        validate(tokens, newtokens);

        lexer.setDocument(new DocumentImpl(delete));
        newtokens = lexer.getTokenList();
        new Difference(tokens, newtokens).syncDifference();
        validate(tokens, newtokens);
    }

    public void validate(ArrayList<Token> tl1, ArrayList<Token> tl2) {
        assertEquals(tl1.size(), tl2.size());
        for (int i=0; i<tl1.size(); i++) {
            assertEquals(tl1.get(i).getTokenText(), tl2.get(i).getTokenText());
            assertEquals(tl1.get(i).getTokenType(), tl2.get(i).getTokenType());
            assertEquals(tl1.get(i).getLineNumber(), tl2.get(i).getLineNumber());
            assertEquals(tl1.get(i).getCharInLineNumber(), tl2.get(i).getCharInLineNumber());
            assertEquals(tl1.get(i).getStartOffset(), tl2.get(i).getStartOffset());
            assertEquals(tl1.get(i).getEndOffset(), tl2.get(i).getEndOffset());
        }
    }
}
