package UI;

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import conf.Config;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;

import javax.swing.*;

/**
 * Created by noah on 2015-10-18.
 */
public class AppearanceView {

    private JComboBox colourComboBox;
    private JPanel panel1;
    public JLabel title;
    private String[] colors = {"Black and White","Blue","Cyan","Green","Magenta","Red","Yellow"};

    public AppearanceView() {
        title.setText("Appearance");
        for (String color: colors) {
            colourComboBox.addItem(color);
        }
        colourComboBox.setSelectedIndex(Config.getSelectedColor());
    }

    public JPanel getPanel() {
        return panel1;
    }

    public void apply() {
        Config.setSelectedColor(colourComboBox.getSelectedIndex());
        highlightUpdate();
    }

    private void highlightUpdate() {
        final Editor editor = FileEditorManager.getInstance(UnnaturalProgram.getProject()).getSelectedTextEditor();
        if (editor == null) {
            return;
        }
        TokenizedCode code = UnnaturalProgram.getTokenizedCode(editor);
        if (code.isHighlighted()) {
            code.refreshEntropy();
        }
    }
}
