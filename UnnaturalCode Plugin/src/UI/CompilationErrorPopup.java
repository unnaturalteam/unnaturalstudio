package UI;

import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.VisualPosition;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.impl.PsiAwareFileEditorManagerImpl;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.JBPopup;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.ui.popup.JBPopupListener;
import com.intellij.openapi.ui.popup.PopupChooserBuilder;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.psi.*;
import com.intellij.psi.impl.source.PsiFileImpl;
import com.intellij.psi.impl.source.resolve.reference.impl.providers.PsiFileReference;
import com.intellij.ui.awt.RelativePoint;
import com.intellij.ui.components.JBList;
import conf.Config;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.Nullable;
import util.Domain.Token;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;

import javax.swing.*;
import java.util.LinkedList;
import java.util.List;
import com.intellij.util.PsiNavigateUtil;

/**
 * Created by Lyle on 9/25/2015.
 */
public class CompilationErrorPopup {

    public CompilationErrorPopup(Project project) {

        final LinkedList<Token> entlist = getTokens(project);
        if (entlist == null) return;

        DefaultListModel<String> listModel = new DefaultListModel<>();
        for (int i=1; i<=entlist.size() && i<=10; i++) {
            String tokenText = StringUtils.abbreviate(entlist.get(i-1).getTokenText(), 20);
            listModel.addElement(i + ") \"" + tokenText
                    + "\" (" + entlist.get(i-1).getTokenType().toLowerCase() + ")");
        }

        final JBList errjlist = new JBList(listModel);
        errjlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JBPopupFactory fact = JBPopupFactory.getInstance();
        final PopupChooserBuilder listPopupBuilder=fact.createListPopupBuilder(errjlist);
        listPopupBuilder.setTitle("Top 10 highest entropy areas");
        listPopupBuilder.setMovable(true);
        listPopupBuilder.setCloseOnEnter(false);
        final CaretModel caretModel = FileEditorManager.getInstance(project).getSelectedTextEditor().getCaretModel();
     //   final PsiFile file = PsiManager.getInstance(project).findFile(FileEditorManager.getInstance(project).getSelectedFiles()[0]);
   //     final PsiFile file = PsiManager.getInstance(project).findFile(FileEditorManager.getInstance(project).get);

        Document document = FileEditorManager.getInstance(project).getSelectedTextEditor().getDocument();
        VirtualFile virtualFile = FileDocumentManager.getInstance().getFile(document);
        final PsiFile file = PsiManager.getInstance(project).findFile(virtualFile);

        listPopupBuilder.setItemChoosenCallback(new Runnable() {
            @Override
            public void run() {
           //     caretModel.moveToOffset(entlist.get(errjlist.getSelectedIndex()).getStartOffset());
                if (file != null) {
                    PsiNavigateUtil.navigate(file.findElementAt(entlist.get(errjlist.getSelectedIndex()).getStartOffset()));
                }
            }
        });
        JBPopup jbPopup=listPopupBuilder.createPopup();
        jbPopup.show(RelativePoint.getNorthEastOf(FileEditorManager.getInstance(project).getSelectedTextEditor().getComponent()));
    }

    /**
     * gets the tokens with the top 10 entropy from current editor window
     * @param project
     * @return list of top 10 entropic tokens
     */
    public static LinkedList<Token> getTokens(Project project) {
        final Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
        if (editor == null) {
            return null;
        }
        TokenizedCode tc = UnnaturalProgram.getTokenizedCode(editor);
        tc.updateTokenList();
        tc.calculateEntropies();

        final LinkedList<Token> entlist = new LinkedList<>();
        for (Token token : tc.getTokenList()) {
            boolean added = false;
            for (int i=0; i<entlist.size(); i++) {
                if (token.getEntropy() > entlist.get(i).getEntropy()) {
                    entlist.add(i, token);
                    added = true;
                    break;
                }
            }
            if (!added)
                entlist.add(token);
        }
        return entlist;
    }


    public static void main(Project project) {
        new CompilationErrorPopup(project);
    }
}
