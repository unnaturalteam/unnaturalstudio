package UI;

import com.intellij.openapi.ui.DialogBuilder;
import com.intellij.openapi.ui.DialogWrapper;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

/*
 * This class Builds the configuration popup that the user interacts with to
 * make changes to UnnaturalStudio's configuration
 */
public class ConfigView {

    public static final int APPEARANCE = 0;
    public static final int SETUP_SERVER = 1;
    public static final int OPT = 2;


    private JPanel contentPane;
    private JList<String> configMenu;
    private JSplitPane splitPane;

    private AppearanceView appearanceView = new AppearanceView();
    private ServerSetupView serverSetupView = new ServerSetupView();
    private OptSettingsView optSettingsView = new OptSettingsView();


    public ConfigView() {

        setupListMenu();
        splitPane.setUI(new BasicSplitPaneUI() {
            public BasicSplitPaneDivider createDefaultDivider() {
                return new BasicSplitPaneDivider(this) {
                    public void setBorder(Border b) {
                    }
                };
            }
        });
        splitPane.setBorder(null);

        final DialogBuilder configDialog = new DialogBuilder();
        configDialog.setTitle("Unnatural Studio Configuration");
        configDialog.centerPanel(contentPane);
        configDialog.setOkOperation(new Runnable() {
            @Override
            public void run() {
                apply();
                configDialog.getDialogWrapper().close(DialogWrapper.OK_EXIT_CODE);
            }
        });
        configDialog.setPreferredFocusComponent(configMenu);
        configDialog.show();
    }



    private void apply() {
        appearanceView.apply();
        serverSetupView.apply();
        optSettingsView.apply();
    }

    private void setupListMenu() {
        DefaultListModel<String> listModel = new DefaultListModel<>();
        listModel.addElement(appearanceView.title.getText());
        listModel.addElement(serverSetupView.title.getText());
        listModel.addElement(optSettingsView.title.getText());

        configMenu.setModel(listModel);
        configMenu.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        configMenu.setFixedCellWidth(100);
        configMenu.setSelectedIndex(0);
        changeView(configMenu.getSelectedIndex());

        configMenu.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                changeView(configMenu.getSelectedIndex());
            }
        });
    }

    private void changeView(int index) {
        switch (index) {
            case APPEARANCE:
                splitPane.setRightComponent(appearanceView.getPanel());
                break;
            case SETUP_SERVER:
                splitPane.setRightComponent(serverSetupView.getPanel());
                break;
            case OPT:
                splitPane.setRightComponent(optSettingsView.getPanel());
                break;
        }
    }
}
