package UI;

import conf.Config;

import javax.swing.*;

/**
 * Created by noah on 2015-10-18.
 */
public class OptSettingsView {

    private JPanel panel1;
    private JComboBox optComboBox;
    public JLabel title;
    private JTextArea optDescription;
    private String descTextPart1 = "You are currently opted-";
    private String descTextPart2 = " for research data collection." +
            " By opting-in, you agree to send anonymous data that will" +
            " be accessible only by researchers.";


    public OptSettingsView() {
        title.setText("Opt-In Settings");
        optComboBox.addItem("Opt-In");
        optComboBox.addItem("Opt-Out");
        optDescription.setOpaque(false);
        update();
    }

    public JPanel getPanel() {
        return panel1;
    }

    public void apply() {
        if (optComboBox.getSelectedIndex() == 0) {
            Config.setOptIn(Config.OPT_IN);
        } else if (optComboBox.getSelectedIndex() == 1) {
            Config.setOptIn(Config.OPT_OUT);
        }

    }

    public void update() {
        if (Config.isOptIn() == Config.OPT_IN) {
            optComboBox.setSelectedIndex(0);
            optDescription.setText(descTextPart1+"in"+descTextPart2);
        } else if (Config.isOptIn() == Config.OPT_OUT) {
            optComboBox.setSelectedIndex(1);
            optDescription.setText(descTextPart1+"out"+descTextPart2);
        }
    }
}
