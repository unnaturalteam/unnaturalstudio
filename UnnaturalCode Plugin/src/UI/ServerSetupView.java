package UI;

import Actions.CompilerListener;
import conf.Config;
import util.Access.RESTServer;
import util.Access.UnnaturalDB;
import util.Domain.Token;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by noah on 2015-10-18.
 */
public class ServerSetupView {
    private JPanel panel1;
    public JLabel title;
    private JFormattedTextField urlTextBox;

    public ServerSetupView() {
        title.setText("Server Setup");
        urlTextBox.setText(Config.getUrl());
        update();
    }

    public JPanel getPanel() {
        return panel1;
    }

    /**
     * apply changes, and if the server has changed, send all historical data
     */
    public void apply() {
        String oldurl = Config.getUrl();
        Config.setUrl(urlTextBox.getText());
        if (!oldurl.equals(Config.getUrl())) {
            try {
                ArrayList<String> histdata = UnnaturalDB.getHistoricalData();
                RESTServer rs = new RESTServer(Config.getUrl());
                for (String filecontent : histdata) {
                    TokenizedCode code = UnnaturalProgram.getTokenizedCode(filecontent);
                    rs.Train(code);
                }
                rs.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void update() {
        urlTextBox.setText(Config.getUrl());
    }
}
