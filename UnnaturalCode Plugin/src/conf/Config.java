package conf;

/**
 * This class holds the variables and methods used by UnnaturalCode to handle the users
 * preferences for both color modes and data research
 * Created by Lyle on 9/25/2015.
 */
public class Config {

    public static final boolean OPT_IN = true;
    public static final boolean OPT_OUT = false;

    private static int[][] codes = {{0,0,0},{0,0,1},{0,1,1},{0,1,0},{1,0,1},{1,0,0},{1,1,0}};

    private static String url = "https://pizza.cs.ualberta.ca/uc/generic/";
    private static boolean optIn = OPT_IN;

    private static int SelectedColor = 5; // Red == 5
    private static int[] ColorCode = codes[SelectedColor];


    public static int getSelectedColor() {
        return SelectedColor;
    }

    public static void setSelectedColor(int selectedColor) {
        SelectedColor = selectedColor;
        setColorCode(selectedColor);
    }

    public static int[] getColorCode() {
        return ColorCode;
    }

    private static void setColorCode(int selectedColor) {
        ColorCode = codes[selectedColor];
    }


    /**
     * Gets the URL for the UnnaturalCodeServer
     * @return string
     */
    public static String getUrl() {
        return url;
    }

    /**
     * Sets URL for UnnaturalCodeServer
     */
    public static void setUrl(String surl) {
        url = surl;
    }

    /**
     * Gets whether User Optin is true or not
     * @return boolean
     */
    public static boolean isOptIn() {
        return optIn;
    }

    /**
     * Sets the User OptIn Preference
     */
    public static void setOptIn(boolean optInOut) {
        optIn = optInOut;
    }
}
