package util.Access;

import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import conf.Config;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import util.Domain.Suggestion;
import util.Domain.TokenizedCode;

import java.io.*;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class is responsible for handling communication between UnnaturalStudio and
 * UnnaturalCode
 * Created by Lyle on 9/23/2015.
 */
public class RESTServer {
    //Skeleton for interactions with the REST server
    //REST specification: https://github.com/orezpraw/unnaturalcode/blob/master/unnaturalcode/http/README.md
    private String user = "uc";
    private String pwd = "ohmieguo8aith9eT1Chou9ti2aeZ3too";
    CloseableHttpClient httpClient;

    /*
     * Constructor for the RESTServer.
     *
     */
    public RESTServer(String url) {
        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(user, pwd);
        provider.setCredentials(AuthScope.ANY, credentials);
        HttpClientBuilder builder = HttpClientBuilder.create();
        builder.setDefaultCredentialsProvider(provider);
        httpClient = builder.build();

    }

    /**
     * Sends the UnnaturalCode server a json string that represents the sourcecode.
     * The goal of this is to train the corpus of the UnnaturalCode server to learn
     * more about Java projects.
     * @param code the TokenizedCode to be used to train the corpus
     */
    public int Train(TokenizedCode code) {
        String jsonstring = code.getJSONArrayString();
        int statuscode = 500;
        /*
        POST /{corpus}/

        Upload a file for training.
        */
        try {
            System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
            System.setProperty("org.apache.commons.logging.simplelog.defaultlog", "trace");

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.addTextBody("s", jsonstring, ContentType.MULTIPART_FORM_DATA);

            HttpPost post = new HttpPost(Config.getUrl());
            post.setEntity(builder.build());

            HttpResponse response = httpClient.execute(post);
            statuscode = response.getStatusLine().getStatusCode();
            if (response.getStatusLine().getStatusCode() > 202) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;

            /*while ((output = br.readLine()) != null) {
                System.out.println(output);
            }*/

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return statuscode;
    }

    /**
     * get the list of suggestions for the provided context
     * @param jsonstring the json string representation of that token and 9 previous tokens (if possible
     *                   more the better)
     * @return list of suggestions returned from the server
     */
    public ArrayList<Suggestion> getSuggestions(String jsonstring) {
        ArrayList<Suggestion> suggestions = new ArrayList<>();
        try {
            System.setProperty("org.apache.commons.logging.Log","org.apache.commons.logging.impl.SimpleLog");
            System.setProperty("org.apache.commons.logging.simplelog.defaultlog","trace");

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.addTextBody("s", jsonstring, ContentType.MULTIPART_FORM_DATA);

            HttpPost post = new HttpPost(Config.getUrl() + "predict/");
            post.setEntity(builder.build());

            HttpResponse response = httpClient.execute(post);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            JsonReader reader = new JsonReader(new InputStreamReader((response.getEntity().getContent())));
            reader.beginObject();
            //System.out.println(reader.nextName());
            reader.nextName();
            reader.beginArray();
            if (reader.hasNext()) {
                while (reader.hasNext()) {
                    reader.beginArray();
                    Suggestion suggestion = new Suggestion(reader.nextDouble());
                    reader.beginArray();
                    while (reader.hasNext()) {
                        String next = reader.nextString();
                        if (next.contains("WHITE_SPACE"))
                            continue;
                        suggestion.addSuggestion(next.substring(next.indexOf(":")+1));
                    }
                    suggestions.add(suggestion);
                    reader.endArray();
                    reader.endArray();
                    //System.out.println(reader.nextString());
                }
                reader.endArray();
            }
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

        return suggestions;
    }


    /**
     * Get all the entropies for all the windows in passed JSON
     * @param jsonstring The JSON representation
     * @return list of entropies of all windows
     */
    public ArrayList<Double> getWEntropy(String jsonstring) {
        ArrayList<Double> entropies = new ArrayList<>();
        try {
            System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
            System.setProperty("org.apache.commons.logging.simplelog.defaultlog", "trace");

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.addTextBody("s", jsonstring, ContentType.MULTIPART_FORM_DATA);

            HttpPost post = new HttpPost(Config.getUrl() + "wxentropy");
            post.setEntity(builder.build());

            HttpResponse response = httpClient.execute(post);

            if (response.getStatusLine().getStatusCode() != 200) {
                System.err.println("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }

            /*BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;

            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }*/

            JsonReader reader = new JsonReader(new InputStreamReader((response.getEntity().getContent())));
            reader.beginObject();
            reader.nextName();
            reader.beginArray();
            while (reader.hasNext()) {
                reader.beginArray();
                if (reader.peek().equals(JsonToken.BEGIN_ARRAY))
                    break;
                reader.nextBoolean();
                entropies.add(reader.nextDouble());
                reader.endArray();
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        if (entropies.size() == 0)
            entropies.add(70.0);
        return entropies;
        //return new Random().nextDouble();
    }

    /**
     * close http connection
     */
    public void close() {
        try {
            httpClient.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


}
