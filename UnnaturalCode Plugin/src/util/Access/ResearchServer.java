package util.Access;

import com.google.gson.Gson;
import conf.Config;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.shield.authc.support.SecuredString;


import java.awt.*;

import static org.elasticsearch.shield.authc.support.UsernamePasswordToken.basicAuthHeaderValue;



/**
 * Created by Lyle on 9/23/2015.
 * This class stores all the the variables and methods needed for communicating
 * to UnnaturalStudio's server used for storing research data.
 */
public class ResearchServer {

    private static String ip = "199.116.235.181";
    private static Client client;
    private static String username = "client";
    private static String password = "client";
    private static String token;

    /**
     * Sends the data to the research server
     *
     * @return boolean that indicated whether the connection was a success or not
     */
    public static boolean sendData(Object obj) {
        if (Config.isOptIn()) {
            try {
                IndexRequest indexRequest = new IndexRequest("research", obj.getClass().getSimpleName());
                indexRequest.source(new Gson().toJson(obj)).putHeader("Authorization", token);
                client.index(indexRequest).actionGet();
            } catch (Exception e) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Send data to research server
     * @param location
     * @param obj
     * @param index
     */
    public static void sendData(String location, Object obj, String index) {
        if (Config.isOptIn()) {
            try {
                IndexRequest indexRequest = new IndexRequest(location, obj.getClass().getSimpleName(), index);
                indexRequest.source(new Gson().toJson(obj)).putHeader("Authorization", token);
                client.index(indexRequest).actionGet();
            } catch (Exception ignored) {}
        }
    }

    /**
     * Sets up the connection with a UnnaturalStudios research server
     *
     * @return boolean that indicated whether the connection was a success or not
     */
    public static boolean establishConnection() {
        token = basicAuthHeaderValue(username, new SecuredString(password.toCharArray()));
        try {
            Thread.currentThread().setContextClassLoader(ResearchServer.class.getClassLoader());
            client = new TransportClient(
                    ImmutableSettings.settingsBuilder()
                            .put("shield.user", username + ":" + password)
                            .put("path.conf", "../ES/config")
                            .build())
                    .addTransportAddress(new InetSocketTransportAddress(ip, 9300))
                    .addTransportAddress(new InetSocketTransportAddress(ip, 9301));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * @param location
     * @param obj
     * @param index
     * @return
     */
    public static GetResponse getData(String location, Object obj, String index) {
        GetResponse response = null;
        try {
            response = client.prepareGet(location, obj.getClass().getSimpleName(), index).putHeader("Authorization", token)
                    .execute()
                    .actionGet();
        } catch (Exception ignored) {
        }
        return response;
    }

    public static void closeConnection() {
        client.close();
    }

    public static String getIp() {
        return ip;
    }

    public static void setIp(String ipAddress) {
        ip = ipAddress;
    }

    public static void setUsername(String username) {
        ResearchServer.username = username;
    }

    public static void setPassword(String password) {
        ResearchServer.password = password;
    }
}
