package util.Access;

import conf.Config;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;
import util.ProjectFileManager;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Lyle on 12/1/2015.
 */
public class UnnaturalDB {
    private static String dblocation;

    /**
     * initializes the db, creating the file if it doesn't exist and creating the table
     */
    public static void initDB() {
        dblocation = ProjectFileManager.getFilePath(UnnaturalProgram.getProject().getProjectFilePath(), 2);
        String query = "CREATE TABLE IF NOT EXISTS FileData(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "content TEXT)";
        update(query);
    }

    /**
     * store the contents of a file into the database
     * @param content The string to be stored
     */
    public static void storeFile(String content) {
        update("INSERT INTO FileData(content) VALUES('" + content + "')");
        TokenizedCode code = UnnaturalProgram.getTokenizedCode(content);
        RESTServer rs = new RESTServer(Config.getUrl());
        rs.Train(code);
        rs.close();
    }

    /**
     * get all the files stored in the database
     * @return the list of string of file contents
     */
    public static ArrayList<String> getHistoricalData() {
        ArrayList<String> historicaldata = new ArrayList<String>();
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            System.err.println("Could not find sqlite JDBC library");
        }
        try {
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + dblocation + "\\compilation_record.db");
            String query = "SELECT content FROM FileData";
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                historicaldata.add(rs.getString("content"));
            }
            rs.close();
            stmt.close();
            con.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return historicaldata;
    }

    /**
     * conveniance function for updates to the DB
     * @param query the SQL query
     */
    private static void update(String query) {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            System.err.println("Could not find sqlite JDBC library");
        }
        try {
            Connection con = DriverManager.getConnection("jdbc:sqlite:" + dblocation + "\\compilation_record.db");
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            stmt.close();
            con.close();
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }
}
