package util;

import util.Domain.Token;

import java.util.ArrayList;

/**
 * A class to find the difference between the new token list and old, and update the old
 * for recalculation
 * Created by Lyle on 10/25/2015.
 */
public class Difference {
    private ArrayList<Token> oldtokens;
    private ArrayList<Token> newtokens;

    /**
     * Difference constructor
     * @param oldtokens the old token list to be updated
     * @param newtokens the new token list from the lexer
     */
    public Difference(ArrayList<Token> oldtokens, ArrayList<Token> newtokens) {
        this.oldtokens = oldtokens;
        this.newtokens = newtokens;
    }

    /**
    * Synchronize the old token list with the new one and reset all affected tokens entropy
     */
    public void syncDifference() {
        int stop;
        int i;
        int j;
        int k;

        //Loop from end of both lists and find where they are no longer equal, then repeat from the front
        for (i=oldtokens.size()-1, stop=newtokens.size()-1; stop >= 0 && i>= 0 && newtokens.get(stop).equals(oldtokens.get(i)); i--, stop--);
        for (j=0, k=0; j<newtokens.size() && k<oldtokens.size() && j<=stop && newtokens.get(j).equals(oldtokens.get(k)); j++, k++);

        //remove out of sync tokens from old list, reset entropies (for deletions)
        for (int index=k; index<=i; index++) {
            oldtokens.remove(k);
            for (int x = k - 19; x < k + 19; x++) {
                if (x < 0 || x >= oldtokens.size())
                    continue;
                oldtokens.get(x).setEntropy(-1);
            }
        }

        //insert new tokens (tokens are initialized to -1 already so no reset needed for insertions)
        for (int index=j; index<=stop; index++, k++) {
            oldtokens.add(k, newtokens.get(index));
        }

        //update the old tokens with their new positional data
        for (int index=0; index < oldtokens.size(); index++) {
            oldtokens.get(index).updateOffsets(newtokens.get(index));
        }
    }
}
