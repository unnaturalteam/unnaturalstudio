package util.Domain;

import java.util.LinkedList;

/**
 * Class for the the object that stores data related to compilation attempts.
 * It stores a list of errors with their locations, whether the user clicked
 * a particular error and/or if they edited it.
 * It uses the hashCode of the text editor window and a combination of line and
 * column to distinguish between errors.
 * Created by timotei on 2015-11-17.
 */
public class ResearchCompilationObject {
    public static LinkedList<ResearchCompilationObject> list = new LinkedList<>();
    public boolean clicked;
    public boolean edited;

    private Integer errorID;
    private Integer row;
    private Integer column;
    private String message;

    /**
     * Constructor for the Research Compilation Object.
     * @param document A hash code
     * @param message The error message
     * @param row The error line. Starts at 1.
     * @param column The error column. Starts at 1.
     */
    public ResearchCompilationObject(Integer document, String message, Integer row, Integer column) {
        this.setErrorID(document);
        this.setColumn(column);
        this.setRow(row);
        this.setMessage(message);

        // Set Default values
        this.setEdited(false);
        this.setClicked(false);
    }


    /**
     * Gets the error ID.
     * @return error ID
     */
    public Integer getErrorID() { return errorID; }

    /**
     * Sets the error ID.
     * @param errorID error id
     */
    public void setErrorID(Integer errorID) { this.errorID = errorID; }

    /**
     * Gets the row position of the error
     * @return row position of the error
     */
    public Integer getRow() {
        return row;
    }

    /**
     * Sets the row position of the row
     * @param row row position of the error
     */
    public void setRow(Integer row) {
        this.row = row;
    }

    /**
     * Gets the column position of the row
     * @return column position of the row
     */
    public Integer getColumn() {
        return column;
    }

    /**
     * Sets the column position of the row
     * @param column column position of the row
     */
    public void setColumn(Integer column) { this.column = column; }

    /**
     * Gets the error message
     * @return error message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the error message
     * @param message error message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isClicked() { return clicked; }

    public void setClicked(boolean clicked) { this.clicked = clicked; }

    public boolean isEdited() { return edited; }

    public void setEdited(boolean edited) { this.edited = edited; }
}
