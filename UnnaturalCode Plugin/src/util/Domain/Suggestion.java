package util.Domain;

import java.util.ArrayList;

/**
 * A wrapper class around suggestions. The entropy is unnaturalcode's assessment
 * on the quality of the suggestion (lower is better)
 * Note, lower entropy suggestions tend to be much shorter, so considering how far in
 * the future you want to look can be useful in filtering out suggestions
 *
 * Besides the entropy score, a potentially good way of determining the quality of the
 * suggestion is see how many times it occurs along with every other Suggestion from
 * RESTServer.getSuggestions()
 * Created by Lyle on 11/27/2015.
 */
public class Suggestion {
    private double entropy;
    private ArrayList<String> suggestions;

    /**
     * constructor, initialized with the entropy returned from the REST server
     * @param entropy
     */
    public Suggestion(double entropy) {
        this.entropy = entropy;
        this.suggestions = new ArrayList<String>();
    }

    public void addSuggestion(String suggestion) {
        suggestions.add(suggestion);
    }
    public ArrayList<String> getSuggestions() {
        return suggestions;
    }
    public double getEntropy() {
        return entropy;
    }
}
