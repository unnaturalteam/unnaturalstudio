package util.Domain;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * Token is a class that contains all the neccesiary information to deal with tokens.
 * Tokens are the main unit that is used in finding Entropy because UnnaturalCode deals exclusively
 * with tokens. This also contails information used for code highlighting.
 * Created by Lyle on 9/25/2015.
 */
public class Token {
    private String tokentext;
    private String tokentype;
    private int startoffset;
    private int endoffset;
    private int lineno;
    private int collineno;
    private double entropy;

    public Token(String tokentext, String type, int startoffset, int lineno, int collineno) {
        this.tokentext = tokentext;
        this.startoffset = startoffset;
        this.endoffset = startoffset + tokentext.length();
        this.lineno = lineno;
        this.collineno = collineno;
        this.tokentype = type;
        this.entropy = -1;
    }

    /**
     * @return the token text
     */
    public String getTokenText() {
        return tokentext;
    }

    /**
     * @return the token type
     */
    public String getTokenType() {
        return tokentype;
    }

    /**
     * @return the char offset for the start of the token
     */
    public int getStartOffset() {
        return startoffset;
    }
    /**
     * @return the char offset for the end of the token
     */
    public int getEndOffset() {
        return endoffset;
    }

    /**
     * @return the line number the token is on
     */
    public int getLineNumber() {
        return lineno;
    }

    /**
     * @return the position in the line
     */
    public int getCharInLineNumber() {
        return collineno;
    }

    /**
     * @return the entropy of this token
     */
    public double getEntropy() {return entropy;}

    /**
     * @param entropy the entropy to set
     */
    public void setEntropy(double entropy) {this.entropy = entropy;}

    /**
     * Update the token with the offset data of the passed token
     * @param token the token with the offset data
     */
    public void updateOffsets(Token token) {
        this.startoffset = token.getStartOffset();
        this.endoffset = token.getEndOffset();
        this.lineno = token.getLineNumber();
        this.collineno = token.getCharInLineNumber();
    }

    /**
     * @return a string representation of the token (debug purposes only)
     */
    public String toString() {
        return tokentext + ":" + tokentype + " at " + lineno + "-" + collineno +
                "(" + getStartOffset() + "-" + getEndOffset() + ")" + "|" + getEntropy();
    }

    /**
     * Gets the JSON representation of this token
     * @return the JSON String
     */
    public String toJSON() {
        String text = StringEscapeUtils.escapeJava(getTokenText());
        return "{\"end\": [ " + getLineNumber() + ", " + (getCharInLineNumber() + getTokenText().length()-1) + " ]," +
                "\"start\": [ " + getLineNumber() + ", " + getCharInLineNumber() + " ]," +
                "\"type\": \"" + getTokenType() + "\"," +
                "\"value\": \"" + text + "\"}";
    }

    /**
     * an equals method that makes comparing tokens easier
     * @param token the token to be compared to
     * @return the equality
     */
    public boolean equals(Token token) {
        if (tokentext.equals(token.getTokenText()))
            return true;
        else
            return false;
    }
}
