package util.Domain;

import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.impl.DocumentImpl;
import com.intellij.openapi.editor.markup.*;
import com.intellij.ui.JBColor;
import conf.Config;
import util.Access.RESTServer;
import util.Difference;
import util.Threads.UpdateThread;
import util.UnnaturalJavaLexer;
import com.intellij.util.ui.UIUtil;
import com.intellij.util.ui.UIUtil;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Lyle on 10/8/2015.
 * Tokenized Code handles both the gathering of entropy of tokens and the highlighting
 * of those tokens based on their entropy.
 */
public class TokenizedCode {
    private ArrayList<Token> tokens;
    private LinkedList<RangeHighlighter> rangehls;
    private Editor editor;
    private double maxentropy = 0;
    private double minentropy = 0;
    private String filetext;
    private File file;
    private boolean highlighted = false;

    /**
     * This method is the constructor for Tokenized Code, it will take in an editor
     * and attach a listener that will refresh the highlighting when code is edited.
     */
    protected TokenizedCode(Editor editor) {
        this.editor = editor;
        this.rangehls = new LinkedList<>();
        this.tokens = new ArrayList<>();
    }

    /**
    * Get TokenizedCode from a file, rather than editor. Cannot be highlighted
     */
    public TokenizedCode(File file) {
        this.file = file;
        updateTokenList();
    }

    public TokenizedCode(String filetext) {
        this.filetext = filetext;
        updateTokenList();
    }

    /**
    * Updates the list of tokens from the editor or file, depending on which constructor
    * was used
     */
    public void updateTokenList() {
        if (editor != null) {
            ArrayList<Token> newtokens = updateTokenListFromEditor();

            synchronized (this) {
                new Difference(tokens, newtokens).syncDifference();
            }

        } else if (file != null){
            tokens = updateTokenListFromFile();
        } else {
            tokens = updateTokenListFromString();
        }
    }

    /**
     * get the token list from a String corresponding to the file contents
     * @return ArrayList of Token objects
     */
    private ArrayList<Token> updateTokenListFromString() {
        int i;
        while ((i = filetext.indexOf("\r\n")) != -1) {
            filetext = filetext.substring(0, i) + "\n" + filetext.substring(i + 1);
        }
        Document doc = new DocumentImpl(filetext);
        return new UnnaturalJavaLexer(doc).getTokenList();
    }

    /**
     * get the token list from the Editor window
     * @return ArrayList of Token objects
     */
    private ArrayList<Token> updateTokenListFromEditor() {
        return new UnnaturalJavaLexer(editor.getDocument()).getTokenList();
    }

    /**
     * get the token list from File
     * @return ArrayList of Token objects
     */
    private ArrayList<Token> updateTokenListFromFile() {
        String doctext = "";
        try {
            FileReader freader = new FileReader(file);
            BufferedReader reader = new BufferedReader(freader);
            String line;
            while ((line = reader.readLine()) != null) {
                doctext += line + "\n";
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println("File " + file.getName() + " not found");
        } catch (IOException ioe) {
            System.err.println("io error");
        }
        Document doc = new DocumentImpl(doctext);
        return new UnnaturalJavaLexer(doc).getTokenList();
    }

    /**
     * Runs through all 20 token windows in the file and gets their entropies from
     * the REST server
     */
    public void calculateEntropies() {
        RESTServer rs = new RESTServer(Config.getUrl());

        int start = 0;
        int end = tokens.size()-1;
        boolean update = false;
        for (int i=0; i<tokens.size(); i++) {
            if (tokens.get(i).getEntropy() < 0) {
                start = i;
                update = true;
                break;
            }
        }
        for (int i=tokens.size()-1; i>=0; i--) {
            if (tokens.get(i).getEntropy() < 0) {
                end = i;
                update = true;
                break;
            }
        }

        if (!update)
            return;

        int cend;
        int cstart;
        if (end+19 >= tokens.size())
            cend = tokens.size()-1;
        else
            cend = start + 19;
        if (start-19 <= 0)
            cstart = 0;
        else
            cstart = start - 19;

        ArrayList<Double> entropies = rs.getWEntropy(getJSONArrayString(cstart, cend));

        for (int i = start; i<=end; i++) {
            int count = 0;
            double entropy = 0;
            for (int j = i-19; j<=i && j-cstart<entropies.size(); j++) {
                if (j < 0)
                    continue;
                entropy += entropies.get(j-cstart);
                count++;
            }
            if (count == 0) {
                tokens.get(i).setEntropy(tokens.get(i - 1).getEntropy());
                System.err.println("Not enough windows returned from server, extending last known entropies");
            } else {
                tokens.get(i).setEntropy(entropy / count);
            }
        }

        rs.close();
    }

    /**
    * returns the token list
     * @return the token list
     */
    public ArrayList<Token> getTokenList() {
        return tokens;
    }

    /**
     * get the updated token list without interfering with calculations of entropies
     * used only when entropies do not matter
     * @return the most up-to-date list of tokens
     */
    public ArrayList<Token> getUpdatedTokenList() {
        if (editor != null) {
            return updateTokenListFromEditor();
        } else if (file != null){
            return updateTokenListFromFile();
        } else {
            return updateTokenListFromString();
        }
    }
    /**
    * returns a string with the JSON array for talking to the REST server
     * @return the JSON string representation of tokens
     */
    public String getJSONArrayString() {
        return getJSONArrayString(0, tokens.size()-1);
    }

    /**
     * returns a string with the JSON array for talking to the REST server
     * uses the passed in token list instead of the one in the object instance
     * @param extokens the list of tokens
     * @param start_index the index from token list to start from
     * @param end_index the last (inclusive) index from token list to get the JSON for
     * @return the JSON string representation of tokens
     */
    public static String getJSONArrayString(ArrayList<Token> extokens, int start_index, int end_index) {
        if (extokens.size() == 0 || start_index == end_index)
            return "[]";
        String json = "[" + extokens.get(start_index).toJSON() + "\n";
        for (int i=start_index+1; i<=end_index; i++) {
            json = json + "," + extokens.get(i).toJSON() + "\n";
        }
        return json + "]";
    }
    /**
     * returns a string with the JSON array for talking to the REST server
     * @param start_index the index from token list to start from
     * @param end_index the last (inclusive) index from token list to get the JSON for
     * @return the JSON string representation of tokens
     */
    public String getJSONArrayString(int start_index, int end_index) {
        return getJSONArrayString(tokens, start_index, end_index);
    }


    /**
     * Highlight an individual token
     * @param token the Token to be highlighted
     */
    private void entropyHighlight(Token token) {
        entropyHighlight(token.getEntropy(), token.getStartOffset(), token.getEndOffset());
    }

    /**
    * Highlights the entropy for the whole file, or the current selection if there is one
     */
    public void entropyHighlight() {
        maxentropy = 0;
        minentropy = 70.0;
        for (int i=0; i<tokens.size(); i++) {
            if (tokens.get(i).getEntropy() == 70)
                continue;
            if (tokens.get(i).getEntropy() > maxentropy)
                maxentropy = tokens.get(i).getEntropy();
            if (tokens.get(i).getEntropy() < minentropy)
                minentropy = tokens.get(i).getEntropy();
        }

        if (minentropy == 70.0)
            maxentropy = 70.0;

        for (Token token : tokens) entropyHighlight(token);
        highlighted = true;
    }

    /**
    * Get the token index corresponding to the passed char offset
     * if it cannot be found, returns -1
     * @param offset the character offset from document
     * @return the index corresponding to the offset
     */
    public int getTokenIndexFromOffset(int offset) {
        return getTokenIndexFromOffset(tokens, offset);
    }

    /**
     * search the passed in list instead
     * @param extokens the list ot search
     * @param offset the character offset from document
     * @return the index corresponding to the offset
     */
    public static int getTokenIndexFromOffset(ArrayList<Token> extokens, int offset) {
        for (int i=0; i<extokens.size(); i++) {
            if ((extokens.get(i).getStartOffset() <= offset) && (extokens.get(i).getEndOffset() > offset))
                return i;
        }
        return -1;
    }

    /**
     * Get the context (19 tokens before and after) the given character offset
     * @param offset the character offset
     * @return the list of Tokens in that context
     */
    public ArrayList<Token> getContext(int offset) {
        ArrayList<Token> context = new ArrayList<>();
        int index = getTokenIndexFromOffset(offset);
        for (int i = index - 19; i < index + 19; i++) {
            if (i < 0 || i + 19 >= tokens.size())
                continue;
            context.add(tokens.get(i));
        }
        return context;
    }

    /**
     * Get the 19 token context before the specified character offset
     * TODO change this to static, not dependant on an instance of TokenizedCode
     * @param extokens the list to search
     * @param offset the character offset
     * @return the 19 token context
     */
    public ArrayList<Token> getTokensBefore(ArrayList<Token> extokens, int offset) {
        ArrayList<Token> context = new ArrayList<>();
        int index = getTokenIndexFromOffset(extokens,offset);
        for (int i = index - 19; i < index; i++) {
            if (i < 0 || i + 19 >= extokens.size())
                continue;
            context.add(extokens.get(i));
        }
        return context;
    }

    /**
     * Get the 19 token context after the specified character offset
     * TODO change this to static, not dependant on an instance of TokenizedCode
     * @param extokens the list to search
     * @param offset the character offset
     * @return the 19 token context
     */
    public ArrayList<Token> getTokensAfter(ArrayList<Token> extokens, int offset) {
        ArrayList<Token> context = new ArrayList<>();
        int index = getTokenIndexFromOffset(extokens,offset);
        for (int i = index+1; i <= index + 19; i++) {
            if (i < 0 || i + 19 >= extokens.size())
                continue;
            context.add(extokens.get(i));
        }
        return context;
    }

    /**
     * Goes through the entire editor and removes all highlighting by this TokenizedCode
     */
    public void removeHighlighting() {
        //UnnaturalProgram.removeTokenListener();
        for (RangeHighlighter rangehl : rangehls)
            editor.getMarkupModel().removeHighlighter(rangehl);
        rangehls.clear();
        highlighted = false;
    }

    /**
     * updates the token list and recalculates the entropy
     */
    public void refreshEntropy() {
        UnnaturalProgram.invokeUpdate(new UpdateThread(this));
    }

    /**
     * refreshes the highlighting for the document
     */
    public void refreshHighlighting() {
        removeHighlighting();
        synchronized (this) {
            //UnnaturalProgram.initTokenListener();
            entropyHighlight();
        }

    }

    /**
     * highlights the code based on the normal color scheme.
     * @param entropy the entropy level
     * @param start_offset where highlighting should begin
     * @param end_offset where highlighting should end
     */
    private void entropyHighlight(double entropy, int start_offset, int end_offset) {
        TextAttributes ta = new TextAttributes();

        int entropyColorInt;
        int setColorInt;
        if (!UIUtil.isUnderDarcula()) {
            entropyColorInt = (int) (255-(100*(entropy-minentropy)/(maxentropy-minentropy)));
            setColorInt = 255;
        }
        else {
            // Colors are inverted in Darcula, so entropyColorInt and setColorInt are switched
            entropyColorInt = 0;
            setColorInt = (int) ((100*(entropy-minentropy)/(maxentropy-minentropy)));
        }

        LinkedList<Integer> rgbList = new LinkedList<>();
        for (int i=0; i<3; i++) {
            if (Config.getColorCode()[i] == 0){ rgbList.add(entropyColorInt);}
            else {rgbList.add(setColorInt);}
        }

        float[] hsbValues = JBColor.RGBtoHSB(rgbList.get(0), rgbList.get(1), rgbList.get(2), null);
        Color color = Color.getHSBColor(hsbValues[0],hsbValues[1],hsbValues[2]);
        JBColor jbcolor = new JBColor(color,color);
        ta.setBackgroundColor(jbcolor);

        rangehls.add(editor.getMarkupModel().addRangeHighlighter(start_offset, end_offset, HighlighterLayer.SELECTION - 1, ta, HighlighterTargetArea.EXACT_RANGE));
    }

    /**
     * reset all the entropies to -1 to force a complete recalculation
     */
    public void resetEntropies() {
        for (Token token: tokens)
            token.setEntropy(-1);
    }
    /**
    * get the raw text of the Token list
     * @return the String containing the raw text
     */
    public String toString() {
        String ret = "";
        for (Token token : tokens) {
            ret += token.getTokenText();
        }
        return ret;
    }

    /**
     * checks whether the highlighting is currently active
     * @return the highlighting state
     */
    public boolean isHighlighted() {
        return highlighted;
    }
}
