package util.Domain;

import Actions.CompilerListener;
import com.intellij.openapi.compiler.CompilerManager;
import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.LogicalPosition;
import com.intellij.openapi.editor.event.*;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import util.Research.ResearchObject;
import util.Threads.DelayedUpdateThread;
import util.Threads.UpdateThread;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Lyle on 10/11/2015.
 */
public class UnnaturalProgram {
    private static HashMap<Editor, TokenizedCode> codemap = new HashMap<>();
    private static Project project;
    private static KeyListener listener = null;
    private static CaretListener cl;
    private static KeyListener compileErrorKeyListener = null;
    private static CaretListener compileErrorCaretListener = null;

    private static Integer thread_update_lock = 0;
    private static UpdateThread next_update = null;

    /**
     * Starts the next update thread
     */
    public static void startNextUpdate() {
        synchronized (thread_update_lock) {
            thread_update_lock--;
            if (next_update != null) {
                next_update.start();
                next_update = null;
            }
        }
    }

    /**
     * invokes or queues up the given thread
     * @param thread the thread to be run
     */
    public static void invokeUpdate(UpdateThread thread) {
        synchronized (thread_update_lock) {
            if (thread_update_lock == 0) {
                thread.start();
            } else {
                if (next_update != null)
                    thread_update_lock--;
                next_update = thread;
            }
            thread_update_lock++;
        }
    }

    /**
     * Cancel a currently queued update
     */
    public static void cancelUpdate() {
        synchronized (thread_update_lock) {
            if (next_update != null)
                thread_update_lock--;
            next_update = null;
        }
    }

    /**
     * Create and register the compiler listener
     */
    public static void initCompilerListener() {
        CompilerListener compilationListener = new CompilerListener();
        CompilerManager.getInstance(project).addCompilationStatusListener(compilationListener);

    }

    /**
     * initialize the key press and caret listeners
     */
    public static void initTokenListener() {
        if (listener == null) {
            listener = new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
                    TokenizedCode code = codemap.get(editor);
                    invokeUpdate(new DelayedUpdateThread(code, 100, code.isHighlighted()));
                }
            };
        }

        Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
        try {
            editor.getContentComponent().addKeyListener(listener);
        } catch (NullPointerException npe) {
            System.err.println("null pointer error on adding key listener");
        }
    }

    /**
     * remove the key press and caret listeners
     */
    public static void removeTokenListener() {
        if (listener == null)
            return;
        Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
        try {
            editor.getContentComponent().removeKeyListener(listener);
        } catch (NullPointerException npe) {
            System.err.println("null pointer error on removing key listener");
        }
    }

    /**
     * initialize listener for compiler errors (for research)
     */
    public static void initCompileErrorListener() {
        final CaretModel caretModel = FileEditorManager.getInstance(project).getSelectedTextEditor().getCaretModel();

        if (compileErrorKeyListener == null) {
            compileErrorKeyListener = new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
                    Integer column = caretModel.getCurrentCaret().getLogicalPosition().column + 1;
                    Integer line = caretModel.getCurrentCaret().getLogicalPosition().line + 1;

                    Integer hashCode = editor.hashCode();
                    Integer i = 0;
                    LinkedList<ResearchCompilationObject> list = ResearchObject.getInstance().getErrors();
                    while (list.size() > i) {
                        ResearchCompilationObject match = list.get(i);

                        if (hashCode.intValue() == match.getErrorID().intValue() & match.getColumn().intValue() == column.intValue() & match.getRow().intValue() == line.intValue())
                        {
                            match.setEdited(true);
                            list.set(i, match);
                        }

                        i++;
                    }
                    ResearchObject.getInstance().setErrors(list);

                }
            };
        }

        // This is sort of a hack. If the caret remains in the place where an error might occur, then the stored
        // error's property that check if its clicked is automatically set to true
        caretModel.moveToLogicalPosition(new LogicalPosition(1,1));

        compileErrorCaretListener = new CaretAdapter() {
            @Override
            public void caretPositionChanged(CaretEvent e) {
                Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
                Integer hashCode = editor.hashCode();
                Integer i = 0;
                Integer column = e.getNewPosition().column + 1;
                Integer line = e.getNewPosition().line + 1;
                //System.out.println("---------------------------");
                LinkedList<ResearchCompilationObject> list = ResearchObject.getInstance().getErrors();
                while (list.size() > i) {
                    ResearchCompilationObject match = list.get(i);
                    //System.out.println("Hash code (caret first, then mine " + hashCode + "; " + match.getErrorID());
                    //System.out.println("Comparing the caret (" + line + "," +column + ") with the error location (" + match.getRow() + "," + match.getColumn() + ")");
                    if (hashCode.intValue() == match.getErrorID().intValue() & match.getColumn().intValue() == column.intValue() & match.getRow().intValue() == line.intValue()) {
                        match.setClicked(true);
                        list.set(i, match);
                    }
                    i++;
                }
                ResearchObject.getInstance().setErrors(list);
            }
        };
        caretModel.addCaretListener(compileErrorCaretListener);
        Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
        try {
            editor.getContentComponent().addKeyListener(compileErrorKeyListener);
        } catch (NullPointerException npe) {
            System.err.println("null pointer error on adding key listener");
        }
    }

    /**
     * remove the compile error listener
     */
    public static void removeCompileErrorListener() {
        if (compileErrorKeyListener == null)
            return;
        Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
        CaretModel caretModel = FileEditorManager.getInstance(project).getSelectedTextEditor().getCaretModel();
        try {
            editor.getContentComponent().removeKeyListener(compileErrorKeyListener);
            caretModel.removeCaretListener(compileErrorCaretListener);
        } catch (NullPointerException npe) {
            System.err.println("null pointer error on removing key listener");
        }
    }

    /**
     * get current Project
     * @return the current Project object
     */
    public static Project getProject() {return project;}
    public static Editor getCurrentEditor() {
        return FileEditorManager.getInstance(project).getSelectedTextEditor();
    }
    /**
     * set the current project
     * @param nproject the current project
     */
    public static void setProject(Project nproject) {project = nproject;}

    /**
     * A wrapper around the TokenizedCode constructor for editor
     * It will create a new TokenizedCode object if one doesn't already exist in the codemap
     * @param editor the editor window
     * @return the TokenizedCode object
     */
    public static TokenizedCode getTokenizedCode(Editor editor) {
        TokenizedCode code = codemap.get(editor);
        if (code == null) {
            code = new TokenizedCode(editor);
            codemap.put(editor, code);
        }
        return code;
    }

    /**
     * A wrapper around the TokenziedCode constructor
     * @param filetext the text corresponding to file contents
     * @return the TokenizedCode object
     */
    public static TokenizedCode getTokenizedCode(String filetext) {
        return new TokenizedCode(filetext);
    }

    /**
     * A wrapper around the TokenziedCode constructor
     * @param file the File to be tokenized
     * @return the TokenizedCode object
     */
    public static TokenizedCode getTokenizedCode(File file) {
        return new TokenizedCode(file);
    }
}
