package util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * A simple utility class to manage the reading of files
 * Created by Lyle on 12/1/2015.
 */
public class ProjectFileManager {
    /**
     * A to generate a file path, removing reduce number of items from the path
     * @param filePath the base bath
     * @param reduce the number of elements of the path to remove
     * @return the new filepath
     */
    public static String getFilePath(String filePath, int reduce) {
        String[] parts = filePath.split("/"); // String array, each element is text between dots
        if (parts.length <= 1)
            parts = filePath.split("\\\\");
        String path = "";
        for (int i = 0; i < parts.length - reduce; i++) {
            path += parts[i];
            path += "/";
        }
        return path;
    }

    /**
     * read a file's contents at the specified location
     * @param path the path to read from
     * @param encoding the character encoding
     * @return the string with the file contents
     */
    public static String readFile(String path, Charset encoding) {
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            String content = new String(encoded, encoding);
            return content;
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * read in all the java files at the specified path and all subdirectories
     * @param filePath the path to search for java files
     * @return list of strings containing the file contents
     */
    public static ArrayList<String> getJavaFileContents(String filePath) {
        ArrayList<String> files = new ArrayList<String>();
        File folder = new File(getFilePath(filePath,0));
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            for (File listOfFile : listOfFiles) {
                if (listOfFile.isFile() && listOfFile.getName().contains(".java")) {
                    String content = readFile(listOfFile.toString(), Charset.defaultCharset());
                    if (content != null)
                        files.add(content);
                    else
                        System.err.println("Error reading java files");
                } else if (listOfFile.isDirectory()) {
                    files.addAll(getJavaFileContents(getFilePath(listOfFile.getPath(), 0)));
                }
            }
        }
        return files;
    }
}
