package util.Research;

import util.Domain.Token;

/**
 * Created by noah on 2015-11-19.
 */
public class EntropyLocation {
    private boolean clickedOn = false;
    private boolean edited = false;
    private Token token;

    public EntropyLocation(Token token) {
        this.token = token;
    }

    public void setClickedOn(boolean clickedOn) {
        this.clickedOn = clickedOn;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    public void setToken(Token token) {
        this.token = token;
    }
}
