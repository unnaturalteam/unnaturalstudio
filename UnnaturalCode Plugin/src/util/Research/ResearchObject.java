package util.Research;

import UI.CompilationErrorPopup;
import com.intellij.compiler.CompilerMessageImpl;
import com.intellij.openapi.compiler.CompileContext;
import com.intellij.openapi.compiler.CompilerMessage;
import com.intellij.openapi.compiler.CompilerMessageCategory;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import util.Access.ResearchServer;
import util.Domain.ResearchCompilationObject;
import util.Domain.Token;
import util.Domain.UnnaturalProgram;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by noah on 2015-11-15.
 */
public class ResearchObject {
    private boolean succeeded;
    private LinkedList<ResearchCompilationObject> errors = new LinkedList<>();
    private LinkedList<ResearchCompilationObject> oldErrors = new LinkedList<>();
    private ArrayList<SuggestionsPackage> allSuggestions = new ArrayList<>();
    private LinkedList<EntropyLocation> entropyLocations = new LinkedList<>();
    private ArrayList<SourceCode> sourceCode;

    public void setSourceCode(ArrayList<SourceCode> sourceCode) {
        this.sourceCode = sourceCode;
    }

    public ArrayList<SuggestionsPackage> getAllSuggestions(){return allSuggestions;}
    public void setAllSuggestions(ArrayList<SuggestionsPackage> _suggestions) {this.allSuggestions = _suggestions;}

    public ArrayList<SourceCode> getSourceCode() {
        return sourceCode;
    }

    public void setSucceeded(boolean succeeded) {
        this.succeeded = succeeded;
    }

    public LinkedList<ResearchCompilationObject> getErrors() {
        return errors;
    }

    public void setErrors(LinkedList<ResearchCompilationObject> errors) {
        this.errors = errors;
    }

    public LinkedList<ResearchCompilationObject> getOldErrors() {
        return oldErrors;
    }

    public void setOldErrors(LinkedList<ResearchCompilationObject> oldErrors) {
        this.oldErrors = oldErrors;
    }

    public ArrayList<SuggestionsPackage> getAllRequests() {
        return allSuggestions;
    }

    public void setAllRequests(ArrayList<SuggestionsPackage> locations) {
        this.allSuggestions = locations;
    }

    public LinkedList<EntropyLocation> getEntropyLocations() {
        return entropyLocations;
    }

    public void setEntropyLocations(LinkedList<EntropyLocation> entropyLocations) {
        this.entropyLocations = entropyLocations;
    }


    private static ResearchObject instance = null;
    private ResearchObject() {}

    public static ResearchObject getInstance() {
        if(instance == null) {
            instance = new ResearchObject();
        }
        return instance;
    }

    public static void nullObject() {
        instance = new ResearchObject();
    }


    public void buildObject(CompileContext compileContext, Project project) {
        if (compileContext.getMessageCount(CompilerMessageCategory.ERROR) > 0)
        {

            CompilerMessage[] messages = compileContext.getMessages(CompilerMessageCategory.ERROR);
            ResearchObject.getInstance().setOldErrors(ResearchObject.getInstance().getErrors());
            LinkedList<ResearchCompilationObject> listCo = ResearchObject.getInstance().getErrors().size() > 0 ? new LinkedList<ResearchCompilationObject>() : ResearchObject.getInstance().getErrors();

            Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
            for (CompilerMessage message : messages) {
                assert editor != null;
                ResearchCompilationObject co = new ResearchCompilationObject
                        (
                                editor.hashCode()
                                ,message.getMessage()
                                , ((CompilerMessageImpl) message).getLine()
                                , ((CompilerMessageImpl) message).getColumn()
                        );
                listCo.add(co);
            }

            ResearchObject.getInstance().setErrors(listCo);
            ResearchObject.getInstance().setSucceeded(false);

            UnnaturalProgram.initCompileErrorListener();
        }
        else {
            ResearchObject.getInstance().setSucceeded(true);
            // Only on successful compilation will we clear the old errors
            // We do this because we need to keep track of old errors to know
            // whether the user click and/or edited them
            ResearchObject.getInstance().setErrors(new LinkedList<ResearchCompilationObject>());
        }

        LinkedList<EntropyLocation> entropyLocations =  new LinkedList<>();
        LinkedList<Token> tokens = CompilationErrorPopup.getTokens(project);
        if (tokens != null) {
            for (int i=1; i<=tokens.size() && i<=10; i++) {
                entropyLocations.add(new EntropyLocation(tokens.get(i)));
            }
        }


        ArrayList<File> javaFiles = getFileNames(project.getBasePath());
        ArrayList<SourceCode> sourceCodeArrayList = new ArrayList<>();
        if (javaFiles != null) {
            for (File file: javaFiles) {
                SourceCode sc = new SourceCode();
                sc.setFileName(file.getName());
                try {
                    String fileContent = readFile(file.getPath(), Charset.defaultCharset());
                    sc.setFile(fileContent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                sourceCodeArrayList.add(sc);
            }
        }
        ResearchObject.getInstance().setSourceCode(sourceCodeArrayList);
        ResearchObject.getInstance().setEntropyLocations(entropyLocations);
        ResearchServer.sendData(ResearchObject.getInstance());
        ResearchObject.getInstance().setAllSuggestions(new ArrayList<SuggestionsPackage>());
    }

    private ArrayList<File> getFileNames(String directory) {
        File[] files = new File(directory).listFiles();
        ArrayList<File> javaFiles = new ArrayList<>();
        if (files == null) {
            return null;
        }
        for (File i: files) {
            if (i.getName().endsWith(".java")) {
                javaFiles.add(i);
            }
            ArrayList<File> newJavaFiles = getFileNames(i.getPath());
            if (newJavaFiles != null) {
                for (File e: newJavaFiles) {
                    javaFiles.add(e);
                }
            }
        }
        return javaFiles;
    }

    private static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

}
