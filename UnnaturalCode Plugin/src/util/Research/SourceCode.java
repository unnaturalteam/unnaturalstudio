package util.Research;

import java.util.ArrayList;

/**
 * Created by noah on 2015-11-19.
 */
public class SourceCode {
    private String fileName = "";
    private String file = "";

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
