package util.Research;

import util.Domain.Suggestion;
import util.Domain.Token;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by noah on 2015-11-18.
 */
public class SuggestionsPackage {
    private ArrayList<Suggestion> suggestions = new ArrayList<>();
    private ArrayList<Token> tokens_before = new ArrayList<>();
    private ArrayList<Token> tokens_after = new ArrayList<>();
    private Integer location = null;
    private String changes = "";

    public String getChanges() {
        return changes;
    }

    public void setChanges(String changes) {
        this.changes = changes;
    }

    public ArrayList<Suggestion> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(ArrayList<Suggestion> suggestions) {
        this.suggestions = suggestions;
    }

    public ArrayList<Token> getTokens_before() {
        return tokens_before;
    }

    public void setTokens_before(ArrayList<Token> tokens_before) {
        this.tokens_before = tokens_before;
    }

    public ArrayList<Token> getTokens_after() {
        return tokens_after;
    }

    public void setTokens_after(ArrayList<Token> tokens_after) {
        this.tokens_after = tokens_after;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }


}
