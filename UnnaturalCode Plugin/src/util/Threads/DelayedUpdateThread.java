package util.Threads;

import util.Domain.TokenizedCode;

/**
 * An update thread that is delayed in it's execution to allow Intellij to update the editor
 * before performing update
 * Created by Lyle on 11/12/2015.
 */
public class DelayedUpdateThread extends UpdateThread {
    private int delay;
    public DelayedUpdateThread(TokenizedCode code, int delay) {
        this(code, delay, true);
    }
    public DelayedUpdateThread(TokenizedCode code, int delay, boolean highlight) {
        super(code, highlight);
        this.delay = delay;
    }

    public void run() {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException ie) {
            System.err.println("DEBUG: delayed update interrupted");
        }
        super.run();
    }
}
