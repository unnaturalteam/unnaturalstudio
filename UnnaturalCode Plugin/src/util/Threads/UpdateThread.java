package util.Threads;

import com.intellij.openapi.application.ApplicationManager;
import util.Domain.TokenizedCode;
import util.Domain.UnnaturalProgram;

/**
 * A thread that performs an immediate update of the entropy and highlighting
 * Created by Lyle on 11/12/2015.
 */
public class UpdateThread extends Thread {
    protected TokenizedCode code;
    private boolean highlight;

    public UpdateThread(TokenizedCode code) {
        this(code, true);
    }
    public UpdateThread(TokenizedCode code, boolean highlight) {
        this.code = code;
        this.highlight = highlight;
    }

    public void run() {
        code.updateTokenList();
        code.calculateEntropies();
        if (highlight)
            ApplicationManager.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                code.refreshHighlighting();
            }
        });
        UnnaturalProgram.startNextUpdate();
    }
}
