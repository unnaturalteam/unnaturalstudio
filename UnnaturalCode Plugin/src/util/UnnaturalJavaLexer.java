package util;

import com.intellij.lang.java.lexer.JavaLexer;
import com.intellij.openapi.editor.Document;
import com.intellij.pom.java.LanguageLevel;
import com.intellij.psi.tree.IElementType;
import util.Domain.Token;

import java.util.ArrayList;

//super class: https://github.com/JetBrains/intellij-community/blob/master/java/java-psi-impl/src/com/intellij/lang/java/lexer/JavaLexer.java

/**
 * The lexer that gets all the tokens and relevant information
 * Created by Lyle on 9/23/2015.
 */
public class UnnaturalJavaLexer extends JavaLexer {
    private Document document;

    public UnnaturalJavaLexer(Document document) {
        super(LanguageLevel.HIGHEST);
        setDocument(document);
    }

    /**
     * @param document the document object where the text to be lexed is stored
     */
    public void setDocument(Document document) {
        this.document = document;
        super.start(document.getCharsSequence().toString());
    }

    /**
     * build the token list for document text
     * @return the token list
     */
    public ArrayList<Token> getTokenList() {
        ArrayList<Token> tokens = new ArrayList<Token>();
        for (int i=0; i<super.getTokenEnd(); i++) {
            String token = super.getTokenText();
            if (!token.equals("")) {
                IElementType ttype = super.getTokenType();
                String type = "";
                if (ttype != null)
                    type = ttype.toString();
                if (!type.contains("COMMENT")) {
                    int offset = super.getCurrentPosition().getOffset();
                    int lineno = document.getLineNumber(offset);
                    int collineno = offset - document.getLineStartOffset(lineno);
                    tokens.add(new Token(token, type, offset, lineno + 1, collineno));
                }
            }
            super.advance();
        }
        return tokens;
    }
}
